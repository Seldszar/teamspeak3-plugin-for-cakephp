<?php
/**
 * Copyright 2013 Alexandre Breteau (http://seldszar.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
App::uses('Component', 'Controller/Component');

/**
 * TeamSpeak3 adapter for CakePHP 2.x
 *
 * @author Alexandre Breteau 
 * @link http://seldszar.fr
 */
class TeamSpeak3Component extends Component {

	public $settings = array(
		'uri' => null
	);

	/**
	 * TeamSpeak3 adapter instance
	 *
	 * @var TeamSpeak3_Adapter_Abstract
	 * @var TeamSpeak3_Node_Abstract
	 */
	protected $adapter;

	public function __construct(ComponentCollection $collection, $settings = array()) {
		parent::__construct($collection, $settings);
		$this->adapter = TeamSpeak3::factory($this->settings['uri']);
	}

	public function __get($name) {
		return $this->adapter->{$name};
	}

	public function __set($name, $value) {
		$this->adapter->{$name} = $value;
	}

	public function __call($name, $arguments) {
		return call_user_func_array(array($this->adapter, $name), $arguments);
	}
}
