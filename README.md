# TeamSpeak3 Plugin for CakePHP

## Installation

### Getting plugin

Clone this source code from this repository into your Plugin/TeamSpeak3 directory.

You should get the following Plugin directory :

    /app
        ...
        /Plugin
            ...
            /TeamSpeak3
                ...

### Configuration

In Config/bootstrap.php

    Cake::loadPlugin(array(
        'TeamSpeak3' => array(
            'bootstrap' => true
        )
    ));

In your controller :

    public $components = array(
        'TeamSpeak3.TeamSpeak3' => array(
            'uri' => YOUR_SERVER_URI_ADDRESS
        )
    );

## Usage

This component is a simple adapter of the [TeamSpeak3 framework](http://addons.teamspeak.com/directory/tools/integration/TeamSpeak-3-PHP-Framework.html). It's use the [following documentation](http://docs.planetteamspeak.com/ts3/php/framework/).

You can access to the library by this component using this instruction :

    $this->TeamSpeak3-> ...
